using System;
using System.Threading;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Benchmark
{
    public class GenerationTask : ITask
    {
        private const Int32 _TEST_SIZE = 1000000;
        private IBoardGenerator _generator;

        public GenerationTask(IBoardGenerator generator)
        {
            _generator = generator;
        }

        public void Execute(IReporter reporter)
        {
            reporter.ReportProgressNew($"Generating {_TEST_SIZE} boards..", 0, _TEST_SIZE);
            for (Int32 i = 0; i < _TEST_SIZE; i++)
            {
                reporter.ReportProgressUpdate($"Generating {_TEST_SIZE} boards..", i + 1);
                _generator.Next();
            }
        }
    }
}
