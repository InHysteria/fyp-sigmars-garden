using Autofac;

namespace SigmarsGarden.Benchmark
{
    class Program
    {
        public static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<System.Random>().SingleInstance().AsSelf();
            builder.RegisterType<Engine.StaticBoardGenerator>().SingleInstance().AsSelf();
            builder.RegisterType<Engine.BoardGenerator>().SingleInstance().AsSelf().As<Engine.IBoardGenerator>();
            builder.RegisterType<Engine.GameEngine>().InstancePerDependency().As<Engine.IGameEngine>();

            //Agents
            //builder.RegisterType<Engine.RandomAgent>().As<Engine.IAgent>();
            builder.RegisterType<Engine.NaiveAgent>().As<Engine.IAgent>();
            builder.RegisterType<Engine.QuickNaiveAgent>().As<Engine.IAgent>();
            //builder.RegisterType<Engine.DFSAgent>().As<Engine.IAgent>().AsSelf();
            //builder.RegisterType<Engine.DFSAgentWithNaiveHeuristics>().As<Engine.IAgent>().AsSelf();
            builder.RegisterType<Engine.MaximumUnlockAgent>().As<Engine.IAgent>().AsSelf();
            //builder.RegisterType<Engine.DFSAgentWithMaxUnlockHeuristic>().As<Engine.IAgent>().AsSelf();

            builder.RegisterType<Application>().AsSelf();
            builder.RegisterType<ConsoleReporter>().As<IReporter>();

            //Tasks
            //builder.RegisterType<SolvabilityTask>().As<ITask>();
            //builder.RegisterType<TrainingGenerationTask>().As<ITask>();
            builder.RegisterType<SolvingTask>().As<ITask>();
            //builder.RegisterType<GenerationTask>().As<ITask>();

            using (IContainer container = builder.Build())
            using (ILifetimeScope scope = container.BeginLifetimeScope())
                scope
                    .Resolve<Application>()
                    .Run();
        }
    }
}
