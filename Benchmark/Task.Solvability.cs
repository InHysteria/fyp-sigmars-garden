using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Benchmark
{
    public class SolvabilityTask : ITask
    {
        private const Int32 _TEST_COUNT = 500;
        private const String _CYCLE_PATH = "Cycles.csv";

        private IGameEngine _engine;
        private DFSAgent _agent;

        public SolvabilityTask(IGameEngine engine, DFSAgent agent)
        {
            _engine = engine;
            _agent = agent;
        }

        public void Execute(IReporter reporter)
        {
            List<(Boolean solved, Int32 cycles)> results = new();

            reporter.ReportMessage($"Generating {_TEST_COUNT} boards, solving them with the DFS agent, and writting the results to disk.");
            reporter.ReportProgressNew(" Boards explored", 0, _TEST_COUNT);

            for (Int32 i = 0; i < _TEST_COUNT; i++)
            {
                _engine.NewGame();
                _agent.Solve(_engine);

                results.Add((_agent.SolutionFound, _agent.CyclesForLastSolution));
                reporter.ReportProgressUpdate(" Boards explored", i);
            }

            reporter.ReportMessage($"  Solvable boards: {results.Count(x => x.solved)}/{_TEST_COUNT} = {(Double)results.Count(x => x.solved)/(Double)_TEST_COUNT*100}%");
            reporter.ReportMessage($"  Cycle data written to {_CYCLE_PATH}");
            File.WriteAllText(_CYCLE_PATH, String.Join("\n", results.Where(x => x.solved).Select(x => x.cycles.ToString())));
        }
    }
}
