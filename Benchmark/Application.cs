using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace SigmarsGarden.Benchmark
{
    public class Application
    {
        private Func<String, Stopwatch, IReporter> _reporterFactory { get; }
        private IEnumerable<ITask> _tasks { get; }

        public Application(
            Func<String, Stopwatch, IReporter> reporterFactory,
            IEnumerable<ITask> tasks
        )
        => (_reporterFactory, _tasks) = (reporterFactory, tasks);

        public void Run()
        {
            foreach (ITask task in _tasks)
            {
                Stopwatch watch = new Stopwatch();
                IReporter reporter = _reporterFactory(task.GetType().Name, watch);
                reporter.ReportMessage("Initialising..");
                watch.Start();
                task.Execute(reporter);
                watch.Stop();
                reporter.ReportMessage("Completed.");
            }
        }
    }
}
