using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Benchmark
{
    public class SolvingTask : ITask
    {
        private const Int32 _TEST_COUNT = 20;

        private IGameEngine _engine;
        private IEnumerable<IAgent> _agents;
        private StaticBoardGenerator _board;

        public SolvingTask(IGameEngine engine, IEnumerable<IAgent> agents, StaticBoardGenerator board)
        {
            _engine = engine;
            _agents = agents;
            _board = board;
        }

        public void Execute(IReporter reporter)
        {
            reporter.ReportMessage($"Testing {_agents.Count()} agents over {_TEST_COUNT} boards.");

            List<Dictionary<IAgent, Boolean>> results = Enumerable
                .Range(0, _TEST_COUNT)
                .Select(_ => new Dictionary<IAgent, Boolean>())
                .ToList();

            Int32 nameLeft = _agents.Max(a => a.GetType().Name.Length) + 2;

            foreach (IAgent agent in _agents)
            {
                Int32 i = 0;
                String name = agent.GetType().Name.PadLeft(nameLeft);

                _board.Reset();
                reporter.ReportProgressNew(name, 0, _TEST_COUNT);

                foreach (Dictionary<IAgent, Boolean> result in results)
                {
                    _engine.LoadGame(_board.Next());
                    using (IEnumerator<Move> solution = agent.Solve(_engine))
                        while (solution.MoveNext())
                            _engine.Take(solution.Current);

                    result[agent] = _engine.IsSolved;
                    reporter.ReportProgressUpdate(name, i++);
                }
                reporter.ReportProgressUpdate(name, _TEST_COUNT, force: true);
            }

            reporter.ReportMessage("");
            //reporter.ReportMessage($"Removed {results.RemoveAll(x => !x.Any(r => r.Value))} boards which no agent solved.");
            reporter.ReportMessage("");
            reporter.ReportMessage("Results..");
            foreach (IAgent agent in _agents)
            {
                String name = agent.GetType().Name.PadLeft(nameLeft);
                reporter.ReportMessage($"{name}: [{String.Join("", results.Select(d => d[agent] ? "O" : "."))}]");
            }
            reporter.ReportMessage("");
            reporter.ReportMessage("Accuracy..");
            foreach (IAgent agent in _agents)
            {
                String name = agent.GetType().Name.PadLeft(nameLeft);
                Int32 successful = results.Where(x => x[agent]).Count();
                reporter.ReportMessage($"{name}: {successful}/{results.Count}, {((Double)successful/(Double)results.Count)*100}%");
            }
        }
    }
}
