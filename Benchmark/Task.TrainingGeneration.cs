using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Benchmark
{
    public class TrainingGenerationTask : ITask
    {
        private Random _random;
        private IGameEngine _engine;
        private List<IAgent> _agents;

        public TrainingGenerationTask(Random random, IGameEngine engine, IEnumerable<IAgent> agents)
        {
            _random = random;
            _engine = engine;
            _agents = agents.ToList();
        }

        public void Execute(IReporter reporter)
        {
            reporter.ReportMessage($"Generating training examples with {_agents.Count} agents.");
            reporter.ReportMessage($"Test runs indefinately, terminate when sufficient examples have been generated.");
            reporter.ReportMessage($"");
            reporter.ReportProgressNew("Active", $"Active agent: <Inactive>");
            reporter.ReportProgressNew("FS", String.Format(new FileSizeFormatProvider(), "Training size: {0:fs}", new System.IO.FileInfo("moves.dat").Length));
            reporter.ReportMessage($"Examples generated..");

            Int32 failed = 0;
            Dictionary<IAgent, Int32> tallies = new Dictionary<IAgent, Int32>();
            foreach (IAgent agent in _agents)
            {
                tallies[agent] = 0;
                reporter.ReportProgressNew(agent.GetType().Name, "  " + agent.GetType().Name + ": " + tallies[agent].ToString());
            }
            reporter.ReportProgressNew("Failed", $"  Failed: 0");

            while (true)
            {
                Int32 a = 0;
                IAgent agent;

                _engine.NewGame();
                do
                {
                    Int32 steps = 0;
                    agent = _agents[a];
                    reporter.ReportProgressUpdate("Active", $"Active agent: [{a}, {agent.GetType().Name}]", force: a > 1);
                    _engine.Retry();

                    using (IEnumerator<Move> solution = agent.Solve(_engine))
                        while (solution.MoveNext() && steps++ < 100000)
                            _engine.Take(solution.Current);

                } while (!_engine.IsSolved && ++a < _agents.Count);

                if (_engine.IsSolved)
                {
                    tallies[agent] += 28;
                    reporter.ReportProgressUpdate(agent.GetType().Name, "  " + agent.GetType().Name + ": " + tallies[agent].ToString());
                    reporter.ReportProgressUpdate("FS", String.Format(new FileSizeFormatProvider(), "Training size: {0:fs}", new System.IO.FileInfo("moves.dat").Length));
                }
                else
                    reporter.ReportProgressUpdate("Failed", $"  Failed: {++failed}");
            }
        }

        //https://stackoverflow.com/questions/128618/file-size-format-provider
        #nullable disable
        public class FileSizeFormatProvider : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                if (formatType == typeof(ICustomFormatter)) return this;
                return null;
            }

            private const string fileSizeFormat = "fs";
            private const Decimal OneKiloByte = 1024M;
            private const Decimal OneMegaByte = OneKiloByte * 1024M;
            private const Decimal OneGigaByte = OneMegaByte * 1024M;

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (format == null || !format.StartsWith(fileSizeFormat))
                {
                    return defaultFormat(format, arg, formatProvider);
                }

                if (arg is string)
                {
                    return defaultFormat(format, arg, formatProvider);
                }

                Decimal size;

                try
                {
                    size = Convert.ToDecimal(arg);
                }
                catch (InvalidCastException)
                {
                    return defaultFormat(format, arg, formatProvider);
                }

                string suffix;
                if (size > OneGigaByte)
                {
                    size /= OneGigaByte;
                    suffix = "GB";
                }
                else if (size > OneMegaByte)
                {
                    size /= OneMegaByte;
                    suffix = "MB";
                }
                else if (size > OneKiloByte)
                {
                    size /= OneKiloByte;
                    suffix = "kB";
                }
                else
                {
                    suffix = " B";
                }

                string precision = format.Substring(2);
                if (String.IsNullOrEmpty(precision)) precision = "2";
                return String.Format("{0:N" + precision + "}{1}", size, suffix);

            }

            private static string defaultFormat(string format, object arg, IFormatProvider formatProvider)
            {
                IFormattable formattableArg = arg as IFormattable;
                if (formattableArg != null)
                {
                    return formattableArg.ToString(format, formatProvider);
                }
                return arg.ToString();
            }

        }
        #nullable restore
    }
}
