using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace SigmarsGarden.Benchmark
{
    public class ConsoleReporter : IReporter
    {
        private Dictionary<String, (Int32 lastProgressPos, Int32 lastProgressLength, Int32 lastProgressMin, Int32 lastProgressMax, DateTime lastProgressUpdate)> _progressBars = new ();

        public Stopwatch Watch { get; }
        public String Name { get; }

        public ConsoleReporter(Stopwatch watch, String name)
        {
            Watch = watch;
            Name = name;
        }

        public void ReportMessage(String message) => Console.WriteLine($"    [{Watch.Elapsed}, {Name}]:> {message}");

        public void ReportError(Exception exception) => ReportError($"{exception.GetType().Name} - {exception.Message}\n        {(exception.StackTrace?.Replace("\n", "\n        ") ?? "<No stack trace>")}");
        public void ReportError(String error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"    [{Watch.Elapsed}, {Name}]:> {error}");
            Console.ResetColor();
        }

        public void ReportProgressNew(String name, String state)
        {
            Console.Write("");
            _progressBars[name] = (Console.CursorTop, 0, -1, -1, DateTime.MinValue);
            ReportProgressUpdate(name, state, true);
            Console.WriteLine();
        }
        public void ReportProgressNew(String name, Int32 min, Int32 max)
        {
            Console.Write("");
            _progressBars[name] = (Console.CursorTop, 0, min, max, DateTime.MinValue);
            ReportProgressUpdate(name, min, true);
            Console.WriteLine();
        }
        public void ReportProgressUpdate(String name, Int32 value, Boolean force = false)
        {
            if (!_progressBars.ContainsKey(name))
            {
                ReportError("Cannot report progress in range without first calling ReportProgressNew(String name, Int32 min, Int32 max)");
                return;
            }
            (Int32 lastProgressPos, Int32 lastProgressLength, Int32 lastProgressMin, Int32 lastProgressMax, DateTime lastProgressUpdate) = _progressBars[name];
            if (lastProgressMin < 0 || lastProgressMax < 0)
            {
                ReportError("Cannot report range progress to a string progress bar.");
                return;
            }

            Double progress = Math.Max(0, Math.Min(1, (Double)(value - lastProgressMin) / (Double)(lastProgressMax - lastProgressMin)));

            Int32 progressBarMaxSize = Console.WindowWidth - $"    [{Watch.Elapsed}, {Name}]:> {name}  ".Length - 4;
            Int32 progressBarCurrentSize = (Int32)Math.Round(progress * progressBarMaxSize);

            ReportProgressUpdate(name, $"{name}  {new String('█', progressBarCurrentSize)}{new String('░', progressBarMaxSize - progressBarCurrentSize)}");
        }
        public void ReportProgressUpdate(String name, String state, Boolean force = false)
        {
            if (!_progressBars.ContainsKey(name))
            {
                ReportError("Cannot report progress without first calling ReportProgressNew(String name, String state) to report a new state.");
                return;
            }
            (Int32 lastProgressPos, Int32 lastProgressLength, Int32 lastProgressMin, Int32 lastProgressMax, DateTime lastProgressUpdate) = _progressBars[name];
            if (!force && (DateTime.Now - lastProgressUpdate).TotalMilliseconds < 10) return; //No need to update so frequently. Console has overhead.
            (Int32 left, Int32 top) currentPos = (Console.CursorLeft, Console.CursorTop);
            String message = $"    [{Watch.Elapsed}, {Name}]:> {state}";

            Console.SetCursorPosition(0, lastProgressPos);
            Console.Write(new String(' ', Math.Max(lastProgressLength, Console.WindowWidth)));
            Console.SetCursorPosition(0, lastProgressPos);
            Console.Write(message);
            Console.SetCursorPosition(currentPos.left, currentPos.top);

            _progressBars[name] = (lastProgressPos, message.Length, lastProgressMin, lastProgressMax, DateTime.Now);
        }
    }
}
