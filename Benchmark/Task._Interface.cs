using System;

namespace SigmarsGarden.Benchmark
{
    public interface ITask
    {
        void Execute(IReporter reporter);
    }
}
