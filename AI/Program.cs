using Autofac;

namespace SigmarsGarden.AI
{
    class Program
    {
        public static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<System.Random>().SingleInstance().AsSelf();
            builder.RegisterType<System.Diagnostics.Stopwatch>().AsSelf();
            builder.RegisterType<Engine.BoardGenerator>().SingleInstance().AsSelf().As<Engine.IBoardGenerator>();
            builder.RegisterType<Engine.GameEngine>().InstancePerDependency().As<Engine.IGameEngine>();

            builder.RegisterType<ConsoleReporter>().As<IReporter>();
            builder
                .RegisterType<QAgent>()
                .WithParameter("qFilePath", "qagent.dat")
                .AsSelf();

            using (IContainer container = builder.Build())
            using (ILifetimeScope scope = container.BeginLifetimeScope())
                scope
                    .Resolve<QAgent>()
                    .Train(scope.Resolve<IReporter>());
        }
    }
}
