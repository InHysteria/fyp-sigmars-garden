using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using SigmarsGarden.Engine;

namespace SigmarsGarden.AI
{
    public class State
    {
        public Byte[] Data { get; }

        protected State(Byte[] data) => Data = data;
        public State(IGameEngine engine) => Data = engine
            .Board
            .Where(t => t.item > PieceState.Empty)
            .OrderBy(t => t.item)
            .ThenBy(t => t.coordinate.y * 100 + t.coordinate.x)
            .Select(t => (
                piece: t.item,
                neighbours: _neighbourOffsets[t.coordinate.y & 1]
                    .Select(o => engine.Board[new Coord(t.coordinate.x + o.x, t.coordinate.y + o.y)])
                    .ToList()
            ))
            .SelectMany(t => new Byte[] {
                (Byte)t.piece,
                (Byte)t.neighbours[0],
                (Byte)t.neighbours[1],
                (Byte)t.neighbours[2],
                (Byte)t.neighbours[3],
                (Byte)t.neighbours[4],
                (Byte)t.neighbours[5]
            })
            .ToArray();

        public void Save(BinaryWriter writer)
        {
            writer.Write(Data.Length);
            writer.Write(Data, 0, Data.Length);
        }
        public static State Load(BinaryReader reader)
        {
            Int32 length = reader.ReadInt32();
            return new State(reader.ReadBytes(length));
        }

        private (Int32 x, Int32 y)[][] _neighbourOffsets = new[]
        {
            new[] { (+1, 0), (+1, -1), ( 0, -1), (-1, 0), ( 0, +1), (+1, +1) },
            new[] { (+1, 0), ( 0, -1), (-1, -1), (-1, 0), (-1, +1), ( 0, +1) }
        };

        public override Boolean Equals(Object? obj) => obj != null && obj is State b && this.Data.Select((d,i) => d == b.Data[i]).All(x => x);
        public override Int32 GetHashCode()
        {
            Int32 hash = 23;
            unchecked
            {
                foreach (Byte data in Data)
                    hash = hash * 31 + data.GetHashCode();
            }

            return hash;
        }
    }
}
