using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using SigmarsGarden.Engine;

namespace SigmarsGarden.AI
{
    public class QAgent : IAgent
    {
        private const Double _LEARN_RATE = 0.5;
        private const Double _GAMMA = 0.5;


        private BinaryFormatter _formatter { get; } = new BinaryFormatter();
        private String _qFilePath { get; }
        private Random _random { get; }
        private IGameEngine _engine { get; }

        public QAgent(String qFilePath, Random random, IGameEngine engine) => (_qFilePath, _random, _engine) = (qFilePath, random, engine);

        public IEnumerator<Move> Solve(IGameEngine engine)
        {
            Dictionary<(State, State), Double> Q = _loadQ();
            if (Q.Count == 0)
                Console.WriteLine("Warning, this agent hasn't been trained and as such will produce bad results. Provide training data or call QAgent.Train() to generate it.");

            Move? next;
            while ((next = engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .OrderByDescending(m => {
                    (State, State) key = (_stateOf(engine), _stateOf(engine.Peek(m)));
                    if (!Q.ContainsKey(key))
                        Q[key] = 0.0;

                    return Q[key];
                })
                .FirstOrDefault())
                != null && !engine.IsSolved && engine.LegalMoves.Count > 0)
                yield return (Move)next;
        }

        public void Train(IReporter reporter)
        {
            Dictionary<(State, State), Double> Q = _loadQ();

            Int32 epoch = 0;
            Int32 solved = 0;
            Int32 qCountLastBlock = Q.Count;
            reporter.ReportMessage("Starting QAgent training process.");
            reporter.ReportMessage($"Training data will be saved to {_qFilePath}.");
            reporter.ReportMessage("");
            reporter.ReportProgressNew("Epoch", "    Epoch: " + epoch);
            reporter.ReportProgressNew("QSize", String.Format(new FileSizeFormatProvider(), "    QSize: {0} entries ({1:fs})", Q.Count, new System.IO.FileInfo(_qFilePath).Length));
            reporter.ReportProgressNew("QChng", "    QSize Change: ...");
            reporter.ReportProgressNew("Succe", "    Success: ...");
            reporter.ReportProgressNew("GameP", "    Current Game Progress: " + _engine.Board.Count(x => x.item > PieceState.Empty));
            while (true)
            {
                reporter.ReportProgressUpdate("Epoch", "    Current Epoch: " + epoch);
                _engine.NewGame();
                State currentState = _stateOf(_engine);
                while (!_engine.IsSolved)
                {
                    //Select random move
                    Move nextMove = _engine
                        .LegalMoves
                        .SelectMany(m => m
                            .Value
                            .Select(c => new Move(m.Key, c)))
                        .Where(m => _engine.IsLegal(m))
                        .OrderByDescending(x => _random.NextDouble())
                        .FirstOrDefault();

                    //If there are no valid moves, fail.
                    if (nextMove == default(Move))
                        break;

                    _engine.Take(nextMove);
                    State nextState = _stateOf(_engine);
                    Double maxQ = Double.MinValue;

                    foreach (Move possibleNextNextMove in _engine
                        .LegalMoves
                        .SelectMany(m => m
                            .Value
                            .Select(c => new Move(m.Key, c)))
                        .Where(m => _engine.IsLegal(m)))
                    {
                        (State, State) skey = (nextState, _stateOf(_engine.Peek(possibleNextNextMove)));
                        if (!Q.ContainsKey(skey))
                            Q[skey] = 0.0;

                        if (Q[skey] > maxQ)
                            maxQ = Q[skey];
                    }

                    (State, State) key = (currentState, nextState);
                    if (!Q.ContainsKey(key))
                        Q[key] = 0.0;

                    Q[key] =
                        ((1 - _LEARN_RATE) * Q[key] +
                        (_LEARN_RATE * (_reward(_engine) + (_GAMMA * maxQ))));

                    currentState = nextState;
                    reporter.ReportProgressUpdate("QSize", String.Format(new FileSizeFormatProvider(), "    QSize: {0} entries ({1:fs})", Q.Count, new System.IO.FileInfo(_qFilePath).Length));
                    reporter.ReportProgressUpdate("GameP", "    Current Game Progress: " + _engine.Board.Count(x => x.item > PieceState.Empty));
                }

                epoch++;
                if (_engine.IsSolved)
                    solved++;

                if (epoch % 100 == 0)
                {
                    //Save Q state
                    _saveQ(Q);

                    reporter.ReportProgressUpdate("Succe", $"    Success: {solved}%");
                    reporter.ReportProgressUpdate("QChng", $"    QSize Change: {Q.Count - qCountLastBlock}");
                    solved = 0;
                    qCountLastBlock = Q.Count;
                }
            }
        }

        private Double _reward(IGameEngine engine) =>
            //Punish for leaving unmatched elements
            engine
                .Board
                .Where(t => PieceState.Salt <= t.item && t.item < PieceState.Vitae)
                .GroupBy(t => t.item)
                .Select(g => g.Count() % 2 * -1.0)
                .Sum(r => r) +

            //Reward for solving the board
            (engine.IsSolved ? 10 : 0);


        private State _stateOf(IGameEngine engine) => new State(engine);

        private void _saveQ(Dictionary<(State, State), Double> q)
        {
            using (FileStream fs = File.Create(_qFilePath))
            using (BinaryWriter writer = new BinaryWriter(fs))
            {
                writer.Write(q.Count);
                foreach (KeyValuePair<(State current, State next), Double> pair in q)
                {
                    pair.Key.current.Save(writer);
                    pair.Key.next.Save(writer);
                    writer.Write(pair.Value);
                }
            }
        }
        private Dictionary<(State, State), Double> _loadQ()
        {
            Dictionary<(State, State), Double> q = new();

            if (!File.Exists(_qFilePath))
                _saveQ(q);

            using (FileStream fs = File.Open(_qFilePath, FileMode.Open))
            using (BinaryReader reader = new BinaryReader(fs))
            {
                Int32 size = reader.ReadInt32();
                for (Int32 i = 0; i < size; i++)
                {
                    State current = State.Load(reader);
                    State next = State.Load(reader);
                    Double value = reader.ReadDouble();

                    q[(current,next)] = value;
                }
            }

            return q;
        }




        //https://stackoverflow.com/questions/128618/file-size-format-provider
        #nullable disable
        public class FileSizeFormatProvider : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                if (formatType == typeof(ICustomFormatter)) return this;
                return null;
            }

            private const string fileSizeFormat = "fs";
            private const Decimal OneKiloByte = 1024M;
            private const Decimal OneMegaByte = OneKiloByte * 1024M;
            private const Decimal OneGigaByte = OneMegaByte * 1024M;

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (format == null || !format.StartsWith(fileSizeFormat))
                {
                    return defaultFormat(format, arg, formatProvider);
                }

                if (arg is string)
                {
                    return defaultFormat(format, arg, formatProvider);
                }

                Decimal size;

                try
                {
                    size = Convert.ToDecimal(arg);
                }
                catch (InvalidCastException)
                {
                    return defaultFormat(format, arg, formatProvider);
                }

                string suffix;
                if (size > OneGigaByte)
                {
                    size /= OneGigaByte;
                    suffix = "GB";
                }
                else if (size > OneMegaByte)
                {
                    size /= OneMegaByte;
                    suffix = "MB";
                }
                else if (size > OneKiloByte)
                {
                    size /= OneKiloByte;
                    suffix = "kB";
                }
                else
                {
                    suffix = " B";
                }

                string precision = format.Substring(2);
                if (String.IsNullOrEmpty(precision)) precision = "2";
                return String.Format("{0:N" + precision + "}{1}", size, suffix);

            }

            private static string defaultFormat(string format, object arg, IFormatProvider formatProvider)
            {
                IFormattable formattableArg = arg as IFormattable;
                if (formattableArg != null)
                {
                    return formattableArg.ToString(format, formatProvider);
                }
                return arg.ToString();
            }

        }
        #nullable restore
    }
}
