using System;

namespace SigmarsGarden.AI
{
    public interface IReporter
    {
        void ReportMessage(String message);

        void ReportError(String error);
        void ReportError(Exception exception);

        void ReportProgressNew(String name, String state);
        void ReportProgressNew(String name, Int32 min, Int32 max);
        void ReportProgressUpdate(String name, String state, Boolean force = false);
        void ReportProgressUpdate(String name, Int32 value, Boolean force = false);
    }
}
