using System;
using System.Reflection;
using Avalonia.Controls;
using Avalonia.Controls.Templates;

namespace SigmarsGarden.Interface
{
    public class ViewLocator : IDataTemplate
    {
        public Boolean SupportsRecycling => false;

        public IControl Build(Object data)
        {
            Type viewType = data
                ?.GetType()
                ?.GetCustomAttribute<ViewModelFor>()
                ?.ViewType
                ?? throw new Exception($"{data?.GetType()?.Name ?? "<NULL>"} cannot be used as a view model, it must be decorated with the {nameof(ViewModelFor)} attribute");

            Control control = (Activator.CreateInstance(viewType) as Control)
                ?? throw new Exception($"{viewType.Name} cannot be used as a view. It must be assignable to Avalonia.Controls.Control");

            control.DataContext = data;
            return control;
        }

        public Boolean Match(object data) => data?.GetType()?.GetCustomAttribute<ViewModelFor>() != null;
    }
}
