using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SigmarsGarden.Interface
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged = delegate { };

        protected Boolean Set<T>(ref T field, T value, IEnumerable<String>? extraNames = null, [CallerMemberName]String callerName = "")
        {
            if (Equals(field, value)) return false;
            field = value;
            RaisePropertyChanged(callerName);

            foreach (String name in extraNames ?? Enumerable.Empty<String>())
                RaisePropertyChanged(name);

            return true;
        }
        protected Boolean Set<T>(Func<T> getter, Action<T> setter, T value, IEnumerable<String>? extraNames = null, [CallerMemberName]String callerName = "")
        {
            if (Equals(getter(), value)) return false;
            setter(value);
            RaisePropertyChanged(callerName);

            foreach (String name in extraNames ?? Enumerable.Empty<String>())
                RaisePropertyChanged(name);

            return true;
        }

        protected void RaisePropertyChanged(String name) => PropertyChanged!(this, new PropertyChangedEventArgs(name));
    }
}
