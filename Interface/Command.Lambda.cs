using System;
using System.Windows.Input;
using System.Threading.Tasks;

namespace SigmarsGarden.Interface
{
    public class LambdaCommand<T> : LambdaCommand
    {
        public LambdaCommand(Action<T> execute, Func<T, Boolean>? canExecute = null) : base(
            (parameter) => execute((T)parameter!),
            (parameter) => parameter is T tParameter && (canExecute?.Invoke(tParameter) ?? true)
        ) {}

        public LambdaCommand(Func<Object?, Task> execute, Func<T, Boolean>? canExecute = null) : base(
            (parameter) => execute((T)parameter!),
            (parameter) => parameter is T tParameter && (canExecute?.Invoke(tParameter) ?? true)
        ) {}
    }

    public class LambdaCommand : ICommand
    {
        public static LambdaCommand Empty { get; } = new LambdaCommand(o => { });
        public event EventHandler? CanExecuteChanged = delegate { };
        public void RaiseCanExecuteChanged() => CanExecuteChanged!(this, new EventArgs());

        private Action<Object?> _execute { get; }
        private Func<Object?, Boolean> _canExecute { get; }

        public LambdaCommand(Action<Object?> execute, Func<Object?, Boolean>? canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute ?? ((parameter) => true);
        }

        public LambdaCommand(Func<Object?, Task> execute, Func<Object?, Boolean>? canExecute = null)
        {
            Boolean isExecuting = false;
            _execute = (async o => {
                isExecuting = true;
                RaiseCanExecuteChanged();

                await execute(o);

                isExecuting = false;
                RaiseCanExecuteChanged();
            });

            _canExecute = o => !isExecuting && (canExecute?.Invoke(o) ?? true);
        }

        public Boolean CanExecute(Object? parameter) => _canExecute(parameter);
        public void Execute(Object? parameter) => _execute(parameter);

    }
}
