using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Autofac;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Logging.Serilog;
using Avalonia.Threading;

namespace SigmarsGarden.Interface
{
    public class Bootstrap : IDisposable, IApplication
    {
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly AppBuilder _appBuilder;
        private readonly Func<IMainViewModel> _mainViewModelFactory;

        public Bootstrap(AppBuilder appBuilder, Func<IMainViewModel> mainViewModelFactory)
        {
            _appBuilder = appBuilder;
            _mainViewModelFactory = mainViewModelFactory;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public Task Main() => Task.Run((Action)_start);

        //Adapted from: https://github.com/AvaloniaUI/Avalonia/wiki/Application-lifetimes
        private void _start() => _appBuilder.Start(_appMain, new String[0]);
        private void _appMain(Application app, string[] args)
        {
            // Do you startup code here
            Window mainWindow = new Window();
            mainWindow.Title = "Final Year Project - Sigmars Garden";
            mainWindow.Content = _mainViewModelFactory();
            mainWindow.Width = 600;
            mainWindow.Height = 480;
            mainWindow.Closed += ((_1,_2) => _cancellationTokenSource.Cancel());
            mainWindow.Show();


            // Start the main loop
            Dispatcher.UIThread.MainLoop(_cancellationTokenSource.Token);
        }

        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
        }
    }
}
