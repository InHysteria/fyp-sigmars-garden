using System;
using Avalonia.Controls;
using Avalonia.Controls.Templates;

namespace SigmarsGarden.Interface
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class ViewModelFor : Attribute
    {
        public Type ViewType { get; }

        public ViewModelFor(Type type)
        {
            if (!typeof(Control).IsAssignableFrom(type)) throw new ArgumentException($"{type.Name} cannot be used as a view. It must be assignable to Avalonia.Controls.Control");

            ViewType = type;
        }
    }
}
