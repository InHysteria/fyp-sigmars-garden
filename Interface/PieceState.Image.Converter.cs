using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Globalization;

using Avalonia;
using Avalonia.Data.Converters;
using Avalonia.Media.Imaging;
using Avalonia.Platform;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Interface
{
    public class PieceStateImageConverter : IValueConverter
    {
        public Boolean IsInactive { get; }
        private static Dictionary<String, IBitmap> _cache = new Dictionary<String, IBitmap>();

        public Object? Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!targetType.IsAssignableFrom(typeof(IBitmap)))
                throw new NotSupportedException("Target type must be assignable from IBitmap.");

            if (!(value is PieceState))
                throw new NotSupportedException("Value must be of type PieceState");

            PieceState state = (PieceState)value;
            String prefix = (parameter as String) ?? "";

            return _get(state switch
            {
                PieceState.Empty => null,
                PieceState.Salt => $"Assets/{prefix}Salt.png",
                PieceState.Air => $"Assets/{prefix}Air.png",
                PieceState.Fire => $"Assets/{prefix}Fire.png",
                PieceState.Water => $"Assets/{prefix}Water.png",
                PieceState.Earth => $"Assets/{prefix}Earth.png",
                PieceState.Vitae => $"Assets/{prefix}Vitae.png",
                PieceState.Mors => $"Assets/{prefix}Mors.png",
                PieceState.Quicksilver => $"Assets/{prefix}Quicksilver.png",
                PieceState.Lead => $"Assets/{prefix}Lead.png",
                PieceState.Tin => $"Assets/{prefix}Tin.png",
                PieceState.Iron => $"Assets/{prefix}Iron.png",
                PieceState.Copper => $"Assets/{prefix}Copper.png",
                PieceState.Silver => $"Assets/{prefix}Silver.png",
                PieceState.Gold => $"Assets/{prefix}Gold.png",

                _ => throw new Exception($"Unknown state '{state}'")
            });
        }
        public Object ConvertBack(Object value, Type targetType, Object paramter, CultureInfo culture) => throw new NotSupportedException("PieceStateImageConverters is oneway.");

        private IBitmap? _get(String? uri) => uri == null
            ? null
            : !_cache.ContainsKey(uri)
            ? _cache[uri] = new Bitmap(uri)
            : _cache[uri];
    }
}
