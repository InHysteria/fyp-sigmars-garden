using System;

using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.Controls;
using Avalonia.Interactivity;

using SigmarsGarden.Interface.ViewModels;

namespace SigmarsGarden.Interface.Views
{
    public class EngineView : UserControl
    {
        public EngineView()
        {
            InitializeComponent();

            AddHandler(BoardCellView.ClickedEvent, CellClicked, RoutingStrategies.Bubble);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void CellClicked(Object? source, BoardCellView.ClickedEventArgs e)
        {
            if (DataContext is EngineViewModel engineViewModel)
            {
                engineViewModel.CellClicked(e.ViewModel);
                e.Handled = true;
            }
        }
    }
}
