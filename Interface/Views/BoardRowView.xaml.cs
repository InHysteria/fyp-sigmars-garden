using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace SigmarsGarden.Interface.Views
{
    public class BoardRowView : UserControl
    {
        public BoardRowView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
