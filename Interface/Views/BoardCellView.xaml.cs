using System;

using Avalonia;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Avalonia.Controls;
using Avalonia.Interactivity;

using SigmarsGarden.Interface.ViewModels;

namespace SigmarsGarden.Interface.Views
{
    public class BoardCellView : UserControl
    {
        public static readonly RoutedEvent<ClickedEventArgs> ClickedEvent = RoutedEvent
            .Register<BoardCellView, ClickedEventArgs>(nameof(Clicked), RoutingStrategies.Bubble);

        public event EventHandler<ClickedEventArgs> Clicked
        {
            add => AddHandler(ClickedEvent, value);
            remove => RemoveHandler(ClickedEvent, value);
        }

        public BoardCellView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void ImagePressed(Object source, PointerPressedEventArgs e)
        {
            if (DataContext is BoardCellViewModel vm)
                RaiseEvent(new ClickedEventArgs(vm)
                {
                    RoutedEvent = ClickedEvent
                });
        }

        public class ClickedEventArgs : RoutedEventArgs
        {
            public BoardCellViewModel ViewModel { get; }

            public ClickedEventArgs(BoardCellViewModel viewModel)
            {
                ViewModel = viewModel;
            }
        }
    }
}
