using Autofac;
using Avalonia;

namespace SigmarsGarden.Interface
{
    class Program
    {
        public static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder
                .RegisterType<Bootstrap>()
                .SingleInstance()
                .As<IApplication>();

            builder
                .Register(scope => AppBuilder
                    .Configure<App>()
                    .UsePlatformDetect())
                .SingleInstance()
                .AsSelf();

            builder.RegisterType<ViewModels.EngineViewModel>().AsSelf().As<IMainViewModel>();
            builder.RegisterType<ViewModels.BoardRowViewModel>().AsSelf();
            builder.RegisterType<ViewModels.BoardCellViewModel>().AsSelf();

            builder.RegisterType<System.Random>().SingleInstance().AsSelf();
            builder.RegisterType<Engine.StaticBoardGenerator>().SingleInstance().As<Engine.IBoardGenerator>();
            builder.RegisterType<Engine.GameEngine>().SingleInstance().As<Engine.IGameEngine>();

            builder.RegisterType<Engine.RandomAgent>().As<Engine.IAgent>();
            builder.RegisterType<Engine.QuickNaiveAgent>().As<Engine.IAgent>();
            builder.RegisterType<Engine.NaiveAgent>().As<Engine.IAgent>();
            builder.RegisterType<Engine.DFSAgent>().As<Engine.IAgent>().AsSelf();
            builder.RegisterType<Engine.DFSAgentWithNaiveHeuristics>().As<Engine.IAgent>().AsSelf();
            builder.RegisterType<Engine.MaximumUnlockAgent>().As<Engine.IAgent>().AsSelf();
            builder.RegisterType<Engine.DFSAgentWithMaxUnlockHeuristic>().As<Engine.IAgent>().AsSelf();

            using (IContainer container = builder.Build())
            using (ILifetimeScope scope = container.BeginLifetimeScope())
                scope
                    .Resolve<IApplication>()
                    .Main()
                    .GetAwaiter()
                    .GetResult();
        }
    }
}
