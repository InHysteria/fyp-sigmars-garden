using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Interface.ViewModels
{
    [ViewModelFor(typeof(Views.EngineView))]
    public class EngineViewModel : ViewModel, IMainViewModel
    {
        private IAgent _agent;

        private IGameEngine _engine { get; }
        private IEnumerator<Move>? _solution { get; set; } = null;
        private Dictionary<Coord, BoardCellViewModel> _cellLookup { get; } = new Dictionary<Coord, BoardCellViewModel>();

        public IAgent Agent
        {
            get => _agent;
            set
            {
                if (Set(ref _agent, value))
                    RecreateSolution();
            }
        }
        public IEnumerable<IAgent> Agents { get; }

        public List<BoardRowViewModel> Rows { get; }

        public LambdaCommand NewGameCommand { get; }
        public LambdaCommand NextMoveCommand { get; }

        public EngineViewModel(
            IGameEngine engine,
            IEnumerable<IAgent> agents,
            Func<IGameEngine, Coord, BoardCellViewModel> cellFactory,
            Func<List<BoardCellViewModel>, Int32, BoardRowViewModel> rowFactory)
        {
            _engine = engine;
            _agent = agents.First();

            Agents = agents;
            Rows =      Enumerable.Range(BoardGenerator.BOARD_SIZE, BoardGenerator.BOARD_SIZE)                  //Create a range of row lengths from the radius to the circumfrence
                .Concat(Enumerable.Range(BoardGenerator.BOARD_SIZE, BoardGenerator.BOARD_SIZE - 1).Reverse())   //Append the same range in reverse but skip the centre row.
                .Select((w,y) => Enumerable
                    .Range(0, w)                                                                        //For each row create a number of cells equal to its width.
                    .Select(i => new Coord(
                        i + (BoardGenerator.BOARD_SIZE / 2 - 1) - (w - BoardGenerator.BOARD_SIZE) / 2,
                        y
                    ))
                    .Select(c => _cellLookup[c] = cellFactory(engine, c))                               //Create a viewmodel for each cell
                    .ToList())
                .Select(rowFactory)
                .ToList();

            NewGameCommand = new LambdaCommand(o => {
                engine.NewGame();

                _selectedCell = null;
                if (_solution != null)
                {
                    if (_cellLookup.ContainsKey(_solution.Current.coord1))
                        _cellLookup[_solution.Current.coord1].Highlighted = false;
                    if (_cellLookup.ContainsKey(_solution.Current.coord2))
                        _cellLookup[_solution.Current.coord2].Highlighted = false;

                    _solution.Dispose();
                }
                _solution = _agent.Solve(_engine);
                if (_solution.MoveNext())
                {
                    if (_cellLookup.ContainsKey(_solution.Current.coord1))
                        _cellLookup[_solution.Current.coord1].Highlighted = true;
                    if (_cellLookup.ContainsKey(_solution.Current.coord2))
                        _cellLookup[_solution.Current.coord2].Highlighted = true;
                }
                foreach (BoardCellViewModel cell in _cellLookup.Values)
                {
                    cell.Selectable = _engine.LegalMoves.ContainsKey(cell.Coordinate) && _engine.LegalMoves[cell.Coordinate].Count > 0;
                    cell.Refresh();
                }
            });

            NextMoveCommand = new LambdaCommand(o => {
                if (_solution == null) return;

                MakeMove(_solution.Current);
                if (_cellLookup.ContainsKey(_solution.Current.coord1))
                {
                    _cellLookup[_solution.Current.coord1].Highlighted = false;
                    _cellLookup[_solution.Current.coord1].Refresh();
                }
                if (_cellLookup.ContainsKey(_solution.Current.coord2))
                {
                    _cellLookup[_solution.Current.coord2].Highlighted = false;
                    _cellLookup[_solution.Current.coord2].Refresh();
                }
                if (_solution.MoveNext())
                {
                    if (_cellLookup.ContainsKey(_solution.Current.coord1))
                        _cellLookup[_solution.Current.coord1].Highlighted = true;
                    if (_cellLookup.ContainsKey(_solution.Current.coord2))
                        _cellLookup[_solution.Current.coord2].Highlighted = true;
                }

                _cellLookup[new Coord(BoardGenerator.BOARD_SIZE - 1, BoardGenerator.BOARD_SIZE - 1)].Refresh(); //See if gold was removed.
            });
        }

        private BoardCellViewModel? _selectedCell = null;

        public void CellClicked(BoardCellViewModel cell)
        {
            if (!_engine.LegalMoves.ContainsKey(cell.Coordinate) || _engine.LegalMoves[cell.Coordinate].Count == 0) return;

            if (_selectedCell == null)
            {
                _selectedCell = cell;
                cell.Selected = true;

                foreach (KeyValuePair<Coord,BoardCellViewModel> p in _cellLookup)
                    p.Value.Selectable = _engine.IsLegal(new Move(cell.Coordinate, p.Key));
            }
            else
            {
                if (_selectedCell != cell)
                {
                    MakeMove(new Move(_selectedCell.Coordinate, cell.Coordinate));
                }

                _selectedCell.Selected = false;
                _selectedCell = null;

                foreach (KeyValuePair<Coord, BoardCellViewModel> p in _cellLookup)
                    p.Value.Selectable = _engine.LegalMoves.ContainsKey(p.Key) && _engine.LegalMoves[p.Key].Count > 0;
            }
        }


        public void MakeMove(Move move)
        {
            _engine.Take(move);

            _cellLookup[new Coord(BoardGenerator.BOARD_SIZE - 1, BoardGenerator.BOARD_SIZE - 1)].Refresh(); //See if gold was removed.
            if (_cellLookup.ContainsKey(move.coord1))
                _cellLookup[move.coord1].Refresh();
            if (_cellLookup.ContainsKey(move.coord2))
                _cellLookup[move.coord2].Refresh();

            foreach (KeyValuePair<Coord, BoardCellViewModel> p in _cellLookup)
                p.Value.Selectable = _engine.LegalMoves.ContainsKey(p.Key) && _engine.LegalMoves[p.Key].Count > 0;
        }

        public void RecreateSolution()
        {
            if (_solution != null)
            {
                if (_cellLookup.ContainsKey(_solution.Current.coord1))
                    _cellLookup[_solution.Current.coord1].Highlighted = false;
                if (_cellLookup.ContainsKey(_solution.Current.coord2))
                    _cellLookup[_solution.Current.coord2].Highlighted = false;

                _solution.Dispose();
            }
            _solution = _agent.Solve(_engine);
            if (_solution.MoveNext())
            {
                if (_cellLookup.ContainsKey(_solution.Current.coord1))
                    _cellLookup[_solution.Current.coord1].Highlighted = true;
                if (_cellLookup.ContainsKey(_solution.Current.coord2))
                    _cellLookup[_solution.Current.coord2].Highlighted = true;
            }
        }
    }
}
