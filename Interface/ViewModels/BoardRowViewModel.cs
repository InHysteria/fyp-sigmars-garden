using System;
using System.Collections.Generic;
using System.Text;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Interface.ViewModels
{
    [ViewModelFor(typeof(Views.BoardRowView))]
    public class BoardRowViewModel : ViewModel
    {
        public List<BoardCellViewModel> Cells { get; }
        public Int32 Top { get; }

        public BoardRowViewModel(List<BoardCellViewModel> cells, Int32 row)
        {
            Cells = cells;
            Top = row * 34;
        }
    }
}
