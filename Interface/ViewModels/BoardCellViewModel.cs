using System;
using System.Collections.Generic;
using System.Text;

using SigmarsGarden.Engine;

namespace SigmarsGarden.Interface.ViewModels
{
    [ViewModelFor(typeof(Views.BoardCellView))]
    public class BoardCellViewModel : ViewModel
    {
        private Boolean _selectable = true;
        private Boolean _selected = false;
        private Boolean _highlighted = false;

        public IGameEngine Engine { get; }
        public Coord Coordinate { get; }
        public Int32 X { get; }
        public Int32 Y { get; }

        public PieceState State => Engine.Board[Coordinate];
        public Boolean Selectable { get => _selectable; set => Set(ref _selectable, value); }
        public Boolean Selected { get => _selected; set => Set(ref _selected, value); }
        public Boolean Highlighted { get => _highlighted; set => Set(ref _highlighted, value); }

        public String Caption => _calculateVar(State, Coordinate.x, Coordinate.y).ToString();

        public BoardCellViewModel(IGameEngine engine, Coord c)
        {
            Engine = engine;
            Coordinate = c;
        }

        public void Refresh()
        {
            RaisePropertyChanged(nameof(State));
            RaisePropertyChanged(nameof(Caption));
        }



        private static Int32 _calculateVar(PieceState state, Int32 x, Int32 y)
        {
            Int32 pos = x - Math.Abs(y - BoardGenerator.BOARD_SIZE + 1) / 2;

            for (Int32 i = 0; i < y; i++)
            {
                pos += (BoardGenerator.BOARD_WIDTH - Math.Abs(i - BoardGenerator.BOARD_SIZE + 1));
            }

            return (Int32)state * 91 + pos + 1;
        }
    }
}
