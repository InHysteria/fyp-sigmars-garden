using System;
using System.Threading.Tasks;

namespace SigmarsGarden.Interface
{
    public interface IApplication
    {
        Task Main();
    }
}
