using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class DFSAgentWithMaxUnlockHeuristic : DFSAgent
    {
        protected override IEnumerator<Move> allLegalMovesOf(IGameEngine engine) =>
            engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .OrderByDescending(m => _score(engine, m))
                .ToList()
                .GetEnumerator();

        private Double _score(IGameEngine engine, Move move) =>
            (Int32)engine.Board[move.coord1] + (Int32)engine.Board[move.coord2]
             //+ ((engine.Board[move.coord1] == PieceState.Salt && engine.Board[move.coord2] != PieceState.Salt) ||
             //   (engine.Board[move.coord2] == PieceState.Salt && engine.Board[move.coord1] != PieceState.Salt) ? -1000 : 0)
             + new[] {
                 _neighbourOffsets[move.coord1.y & 1]
                    .Select(o => new Coord(move.coord1.x + o.x, move.coord1.y + o.y)),
                 _neighbourOffsets[move.coord2.y & 1]
                    .Select(o => new Coord(move.coord2.x + o.x, move.coord2.y + o.y))
             }
                .SelectMany(x => x)
                .Distinct()
                .Count(c => !engine.IsFree(c)) * 50;

        private (Int32 x, Int32 y)[][] _neighbourOffsets = new[]
        {
            new[] { (+1, 0), (+1, -1), ( 0, -1), (-1, 0), ( 0, +1), (+1, +1) },
            new[] { (+1, 0), ( 0, -1), (-1, -1), (-1, 0), (-1, +1), ( 0, +1) }
        };
    }
}
