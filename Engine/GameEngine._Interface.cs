using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public interface IGameEngine
    {
        //I need...
            //A way to know whats on the board
            IHexagonalGrid<PieceState> Board { get; }
                //I think I should abstract out the board..
                    //No
                    //But I should take board generation out of the pervew of the engine.

            //If its completed
            Boolean IsSolved { get; }

            //A count of all free pieces..
            Int32 FreePieces { get; }

            //Assess the legality of any given move
            Boolean IsLegal(Move move);
            Boolean IsFree(Coord coordinate);

            //Game interaction.
            void NewGame();
            void LoadGame(HexagonalGrid<PieceState> board);
            void Retry();
            void Take(Move move);

        //I would like...
            //A list of all legal moves.. (Probably expensive)
            Dictionary<Coord, HashSet<Coord>> LegalMoves { get; }

            //A way to predict state, preferably without making a whole heap of different memory.. (Probably expensive as well)
            IGameEngine Peek(Move move) => Peek(new Move[] { move });
            IGameEngine Peek(IEnumerable<Move> moves);
    }
}
