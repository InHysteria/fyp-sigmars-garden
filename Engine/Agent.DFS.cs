using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class DFSAgent : IAgent
    {
        protected virtual Int32 MAX_CYCLES_BEFORE_ABANDONING => 10000;

        public Int32 CyclesForLastSolution { get; private set; } = 0;
        public Boolean SolutionFound { get; private set; } = false;

        public IEnumerator<Move> Solve(IGameEngine engine)
        {
            SolutionFound = false;
            CyclesForLastSolution = 0;

            if (engine.IsSolved)
            {
                SolutionFound = true;
                return Enumerable.Empty<Move>().GetEnumerator();
            }

            Dictionary<Move, Move?> traversal = new();
            Stack<(IGameEngine engine, Move? move, IEnumerator<Move> iterator)> stack = new();
            stack.Push((engine, null, allLegalMovesOf(engine)));

            while (stack.Count > 0 && !SolutionFound && CyclesForLastSolution < MAX_CYCLES_BEFORE_ABANDONING)
                if (stack.Peek().iterator.MoveNext())
                {
                    CyclesForLastSolution++;
                    Move nextMove = stack.Peek().iterator.Current;
                    IGameEngine nextEngine = stack.Peek().engine.Peek(nextMove);

                    traversal[nextMove] = stack.Peek().move;

                    if (nextEngine.IsSolved)
                        SolutionFound = true;

                    stack.Push((nextEngine, nextMove, allLegalMovesOf(nextEngine)));
                }
                else
                    stack.Pop();

            if (SolutionFound)
            {
                Move? head = stack.Peek().move;
                List<Move> solution = new();

                while (head != null)
                {
                    solution.Insert(0, (Move)head!);
                    head = traversal[(Move)head!];
                }

                return solution.GetEnumerator();
            }
            else
                return Enumerable.Empty<Move>().GetEnumerator();
        }

        protected virtual IEnumerator<Move> allLegalMovesOf(IGameEngine engine) =>
            engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .ToList()
                .GetEnumerator();
    }
}
