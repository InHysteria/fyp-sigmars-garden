using System;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public interface IAgent
    {
        IEnumerator<Move> Solve(IGameEngine engine);
    }
}
