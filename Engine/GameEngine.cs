using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class GameEngine : IGameEngine
    {
        private const Boolean _TRAINING_DO_EMIT = false;
        private const String _TRAINING_PATH = "moves.dat";

        private IBoardGenerator _generator;
        private GameEngine? _parent;
        private HexagonalGrid<PieceState> _root = new HexagonalGrid<PieceState>(0, 0, PieceState.Empty);

        public IHexagonalGrid<PieceState> Board { get; private set; }
        public Boolean IsSolved { get; private set; } = false;

        public Int32 FreePieces => FreePieceSet.Count;

        public Dictionary<Coord, HashSet<Coord>> LegalMoves { get; protected set; } = new Dictionary<Coord, HashSet<Coord>>();
        public HashSet<Coord> FreePieceSet { get; } = new HashSet<Coord>();

        private List<Snapshot> _snapshots = new List<Snapshot>();

        public GameEngine(IBoardGenerator generator, GameEngine? parent = null)
        {
            _generator = generator;
            _parent = parent;


            if (_parent != null)
            {
                Board = new HexagonalGridMask<PieceState>(_parent.Board);

                FreePieceSet.Clear();
                foreach ((Coord coord, PieceState state) in Board)
                    if (state != PieceState.Empty && IsFree(coord))
                        FreePieceSet.Add(coord);

                LegalMoves = FreePieceSet
                    .Select(c => (coord: c, moves: new HashSet<Coord>(_legalMovesWith(c))))
                    .Where(t => t.moves.Count > 0)
                    .ToDictionary(
                        t => t.coord,
                        t => t.moves
                    );
            }
            else
                Board = new HexagonalGridMask<PieceState>(_root);

        }

        public Boolean IsLegal(Move move) =>
            LegalMoves.ContainsKey(move.coord1) &&
            LegalMoves[move.coord1].Contains(move.coord2) &&

            LegalMoves.ContainsKey(move.coord2) &&
            LegalMoves[move.coord2].Contains(move.coord1);

        public void NewGame() => LoadGame(_generator.Next());
        public void LoadGame(HexagonalGrid<PieceState> board)
        {
            if (_parent != null) throw new NotSupportedException("Cannot start or load a game with an engine in a derived state.");
            _root = board;
            Board = new HexagonalGridMask<PieceState>(_root);

            _snapshots.Clear();
            FreePieceSet.Clear();

            foreach ((Coord coord, PieceState state) in Board)
                if (state != PieceState.Empty && IsFree(coord))
                    FreePieceSet.Add(coord);

            LegalMoves = FreePieceSet
                .Select(c => (coord: c, moves: new HashSet<Coord>(_legalMovesWith(c))))
                .Where(t => t.moves.Count > 0)
                .ToDictionary(
                    t => t.coord,
                    t => t.moves
                );

            IsSolved = false;
        }
        public void Retry() => LoadGame(_root);


        public void Take(Move move)
        {
            if (!IsLegal(move)) return; //Move is illegal

            //Remove both pieces from the board
            Board[move.coord1] = PieceState.Empty;
            Board[move.coord2] = PieceState.Empty;
            FreePieceSet.Remove(move.coord1);
            FreePieceSet.Remove(move.coord2);

            _snapshots.Add(Snapshot.CreateFrom(this, move));

            foreach (Coord neighbour in new []
                {
                    _neighbourOffsets[move.coord1.y & 1].Select(o => new Coord(move.coord1.x + o.x, move.coord1.y + o.y)),
                    _neighbourOffsets[move.coord2.y & 1].Select(o => new Coord(move.coord2.x + o.x, move.coord2.y + o.y))
                }
                .SelectMany(x => x)
                .Where(c => Board[c] != PieceState.Empty
                         && !FreePieceSet.Contains(c)
                         && IsFree(c)))
                FreePieceSet.Add(neighbour);

            //I think it may be possible to get away with not recalculating all legal moves, however the logic for doing so escapes me at present.
            LegalMoves = FreePieceSet
                .Select(c => (coord: c, moves: new HashSet<Coord>(_legalMovesWith(c))))
                .Where(t => t.moves.Count > 0)
                .ToDictionary(
                    t => t.coord,
                    t => t.moves
                );

            if (LegalMoves.ContainsKey(new Coord(BoardGenerator.BOARD_SIZE - 1, BoardGenerator.BOARD_SIZE - 1))) //Gold position
                Take(new Move(new Coord(BoardGenerator.BOARD_SIZE - 1, BoardGenerator.BOARD_SIZE - 1), new Coord(BoardGenerator.BOARD_SIZE - 1, BoardGenerator.BOARD_SIZE - 1))); //Remove gold as soon as its possible todo so.

            if (Board.All(t => t.item == PieceState.Empty))
            {
                IsSolved = true;
                if (_parent == null && _TRAINING_DO_EMIT)
                {
                    using (FileStream fs = new FileStream(_TRAINING_PATH, FileMode.Append))
                    using (BinaryWriter writer = new BinaryWriter(fs))
                    foreach (Snapshot snapshot in _snapshots)
                        snapshot.Save(writer);
                }
            }
        }

        //This is slow?
        public IGameEngine Peek(IEnumerable<Move> moves)
        {
            GameEngine clone = new GameEngine(_generator, this);
            foreach (Move move in moves)
                clone.Take(move);

            return clone;
        }




        //See: https://www.redblobgames.com/grids/hexagons/
        private (Int32 x, Int32 y)[][] _neighbourOffsets = new[]
        {
            new[] { (+1, 0), (+1, -1), ( 0, -1), (-1, 0), ( 0, +1), (+1, +1) },
            new[] { (+1, 0), ( 0, -1), (-1, -1), (-1, 0), (-1, +1), ( 0, +1) }
        };
        public Boolean IsFree(Coord coord) => Enumerable
            .Range(1, 6)
            .Select(i => Enumerable
                .Range(-1, 3)
                .Select(m => _neighbourOffsets[coord.y & 1][(i + m) % 6])
                .Select(o => Board[new Coord(coord.x + o.x, coord.y + o.y)])
                .All(s => s == PieceState.Empty))
            .Any(x => x);

        private IEnumerable<Coord> _legalMovesWith(Coord coord) => Board[coord] switch
        {
            PieceState.Empty => Enumerable.Empty<Coord>(),

            PieceState.Salt => FreePieceSet
                .Where(c => c != coord)
                .Where(c => Board[c] == PieceState.Salt ||
                            Board[c] == PieceState.Air ||
                            Board[c] == PieceState.Fire ||
                            Board[c] == PieceState.Water ||
                            Board[c] == PieceState.Earth),
            PieceState.Air => FreePieceSet
                .Where(c => c != coord)
                .Where(c => Board[c] == PieceState.Salt ||
                            Board[c] == PieceState.Air),
            PieceState.Fire => FreePieceSet
                .Where(c => c != coord)
                .Where(c => Board[c] == PieceState.Salt ||
                            Board[c] == PieceState.Fire),
            PieceState.Water => FreePieceSet
                .Where(c => c != coord)
                .Where(c => Board[c] == PieceState.Salt ||
                            Board[c] == PieceState.Water),
            PieceState.Earth => FreePieceSet
                .Where(c => c != coord)
                .Where(c => Board[c] == PieceState.Salt ||
                            Board[c] == PieceState.Earth),

            PieceState.Vitae => FreePieceSet.Where(c => Board[c] == PieceState.Mors),
            PieceState.Mors => FreePieceSet.Where(c => Board[c] == PieceState.Vitae),

            PieceState.Quicksilver => Board
                .Where(t => t.item > PieceState.Quicksilver)
                .OrderBy(t => t.item)
                .Take(1)
                .Where(t => FreePieceSet.Contains(t.coordinate))
                .Select(t => t.coordinate),
            PieceState.Lead => FreePieceSet.Where(c => Board[c] == PieceState.Quicksilver),
            PieceState.Tin => Board.Any(t => t.item == PieceState.Lead)
                ? Enumerable.Empty<Coord>()
                : FreePieceSet.Where(c => Board[c] == PieceState.Quicksilver),
            PieceState.Iron => Board.Any(t => t.item == PieceState.Tin)
                ? Enumerable.Empty<Coord>()
                : FreePieceSet.Where(c => Board[c] == PieceState.Quicksilver),
            PieceState.Copper => Board.Any(t => t.item == PieceState.Iron)
                ? Enumerable.Empty<Coord>()
                : FreePieceSet.Where(c => Board[c] == PieceState.Quicksilver),
            PieceState.Silver => Board.Any(t => t.item == PieceState.Copper)
                ? Enumerable.Empty<Coord>()
                : FreePieceSet.Where(c => Board[c] == PieceState.Quicksilver),

            //Gold is a special case, implementation should remove automatically when possible to remove edge cases.
            PieceState.Gold => Board.Any(t => t.item == PieceState.Silver)
                ? Enumerable.Empty<Coord>()
                : new[] { coord },

            _ => throw new NotSupportedException($"No support for.. {Board[coord]}")
        };
    }
}
