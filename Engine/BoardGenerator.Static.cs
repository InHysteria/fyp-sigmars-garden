using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class StaticBoardGenerator : IBoardGenerator
    {
        public const String BOARD_FILE = "boards.dat";

        public const Int32 GENERATE_BOARD_COUNT = 50000;

        private readonly Int32 _boardSize;
        private readonly PieceState[,] _boards;
        private Int32 _next = 0;

        public StaticBoardGenerator()
        {
            if (!File.Exists(BOARD_FILE))
            {
                Console.WriteLine($"No boards exist, generating {GENERATE_BOARD_COUNT} new boards..");
                using (FileStream fswrite = File.OpenWrite(BOARD_FILE))
                    _generateBoards(fswrite, GENERATE_BOARD_COUNT);
            }

            using (FileStream fs = File.OpenRead(BOARD_FILE))
                (_boardSize, _boards) = _loadBoards(fs);
        }

        public void Reset() => _next = 0;

        public HexagonalGrid<PieceState> Next()
        {
            HexagonalGrid<PieceState> board = new HexagonalGrid<PieceState>(_boardSize*2 - 1, _boardSize*2 - 1, PieceState.Empty);
            Int32 i = 0;
            foreach (Int32 pos in BoardGenerator.BoardPositions)
                board[pos] = _boards[i++, (_next % _boards.GetLength(1))];

            board[new Coord(_boardSize - 1, _boardSize - 1)] = PieceState.Gold;

            _next++;
            return board;
        }

        private static void _generateBoards(Stream output, Int32 count)
        {
            using BinaryWriter writer = new BinaryWriter(output);
            BoardGenerator generator = new BoardGenerator(new Random());

            writer.Write(BoardGenerator.BOARD_SIZE);
            writer.Write(count);
            for (Int32 i = 0; i < count; i++)
            {
                HexagonalGrid<PieceState> board = generator.Next();
                foreach (Int32 pos in BoardGenerator.BoardPositions)
                    writer.Write((Int32)board[pos]);
            }
        }

        private static (Int32,PieceState[,]) _loadBoards(Stream output)
        {
            using BinaryReader reader = new BinaryReader(output);

            Int32 boardSize = reader.ReadInt32();

            Int32 count = reader.ReadInt32();
            PieceState[,] result = new PieceState[90, count];
            for (Int32 boardNo = 0; boardNo < count; boardNo++)
            for (Int32 pieceNo = 0; pieceNo < 90; pieceNo++)
                result[pieceNo, boardNo] = (PieceState)reader.ReadInt32();

            return (boardSize, result);
        }
    }
}
