
using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class DFSAgentWithNaiveHeuristics : DFSAgent
    {
        protected override IEnumerator<Move> allLegalMovesOf(IGameEngine engine) =>
            engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .OrderByDescending(m => _score(engine, m))
                .ToList()
                .GetEnumerator();

        //+Naive
        private const Double _FREE_SCORE = 0.1;
        private const Double _LEGAL_SCORE = 1;
        private const Double _ODD_ELEMENT_PENALTY = -2000; //Avoid leaving elements odd

        private static readonly Dictionary<PieceState, Double> _PIECE_SCORE = new Dictionary<PieceState, Double>
        {
            { PieceState.Empty, 0 },
            { PieceState.Salt, 1 },
            { PieceState.Air, 5 },
            { PieceState.Fire, 5 },
            { PieceState.Water, 5 },
            { PieceState.Earth, 5 },
            { PieceState.Vitae, 10 },
            { PieceState.Mors, 10 },
            { PieceState.Quicksilver, 100 },
            { PieceState.Lead, 100 },
            { PieceState.Tin, 100 },
            { PieceState.Iron, 100 },
            { PieceState.Copper, 100 },
            { PieceState.Silver, 100 },
            { PieceState.Gold, 0 }
        };

        private Double _score(IGameEngine engine, Move move) => _score(engine.Peek(move));
        private Double _score(IGameEngine engine)
        {
            return (engine
                .FreePieces
                * _FREE_SCORE) +

            (engine
                .LegalMoves
                .Sum(m => m
                    .Value
                    .Count)
                * _LEGAL_SCORE) +

            (new[]
                {
                    PieceState.Air,
                    PieceState.Fire,
                    PieceState.Water,
                    PieceState.Earth
                }
                .Sum(s => (engine.Board.Count(t => t.item == s) % 2))
                * _ODD_ELEMENT_PENALTY) +

            (-engine
                .Board
                .Sum(t => _PIECE_SCORE[t.item]));
        }
    }
}
