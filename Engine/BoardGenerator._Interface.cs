using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public interface IBoardGenerator
    {
        HexagonalGrid<PieceState> Next();
    }
}
