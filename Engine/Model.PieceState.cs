using System;

namespace SigmarsGarden.Engine
{
    public enum PieceState
    {
        Empty,
        Salt,        //Can be combined with any of the 4 cardinal element or itself.

        //Carinal elements
        Air,
        Fire,
        Water,
        Earth,

        //Paired elements, each may only be removed by its opposite.
        Vitae,
        Mors,

        //Metals
        Quicksilver, //Can be combined with a metal in order to remove it.
        Lead,
        Tin,
        Iron,
        Copper,
        Silver,
        Gold
    }
}
