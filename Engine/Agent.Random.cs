using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class RandomAgent : IAgent
    {
        public IEnumerator<Move> Solve(IGameEngine engine)
        {
            Random random = new Random();
            Move next;
            while ((next = engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .OrderByDescending(m => random.Next())
                .FirstOrDefault())
                != default(Move) && !engine.IsSolved && engine.LegalMoves.Count > 0)
                yield return (Move)next;
        }
    }
}
