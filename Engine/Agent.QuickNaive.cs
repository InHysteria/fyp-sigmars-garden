using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class QuickNaiveAgent : IAgent
    {
        public Boolean _odd = false;

        public IEnumerator<Move> Solve(IGameEngine engine)
        {
            Move next;
            while ((next = engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .OrderByDescending(m => _score(engine, m))
                .FirstOrDefault())
                != default(Move) && !engine.IsSolved && engine.LegalMoves.Count > 0)
            {
                if ((engine.Board[next.coord1] == PieceState.Salt && engine.Board[next.coord2] != PieceState.Salt) ||
                    (engine.Board[next.coord2] == PieceState.Salt && engine.Board[next.coord1] != PieceState.Salt))
                    _odd = !_odd;

                yield return (Move)next;
            }
        }

        private Double _score(IGameEngine engine, Move move) =>
            (Int32)engine.Board[move.coord1] + (Int32)engine.Board[move.coord2]
             + ((engine.Board[move.coord1] == PieceState.Salt && engine.Board[move.coord2] != PieceState.Salt) ||
                (engine.Board[move.coord2] == PieceState.Salt && engine.Board[move.coord1] != PieceState.Salt) ? (_odd ? 100 : -100) : 0);
    }
}
