using System;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class NaiveAgent : IAgent
    {
        private const Double _FREE_SCORE = 0.1;
        private const Double _LEGAL_SCORE = 1;
        private const Double _ODD_ELEMENT_PENALTY = -2000; //Avoid leaving elements odd

        //Scores relate more to how difficult that the piece is to remove than the value of the piece.
        private static readonly Dictionary<PieceState, Double> _PIECE_SCORE = new Dictionary<PieceState, Double>
        {
            { PieceState.Empty, 0 },
            { PieceState.Salt, 1 },
            { PieceState.Air, 5 },
            { PieceState.Fire, 5 },
            { PieceState.Water, 5 },
            { PieceState.Earth, 5 },
            { PieceState.Vitae, 10 },
            { PieceState.Mors, 10 },
            { PieceState.Quicksilver, 100 },
            { PieceState.Lead, 100 },
            { PieceState.Tin, 100 },
            { PieceState.Iron, 100 },
            { PieceState.Copper, 100 },
            { PieceState.Silver, 100 },
            { PieceState.Gold, 0 }
        };

        public IEnumerator<Move> Solve(IGameEngine engine)
        {
            Move next;
            while ((next = engine
                .LegalMoves
                .SelectMany(m => m
                    .Value
                    .Select(c => new Move(m.Key, c)))
                .Where(m => engine.IsLegal(m))
                .OrderByDescending(m => _score(engine, m))
                .FirstOrDefault())
                != default(Move) && !engine.IsSolved && engine.LegalMoves.Count > 0)
                yield return (Move)next;
        }

        private Double _score(IGameEngine engine, Move move) => _score(engine.Peek(move));
        private Double _score(IGameEngine engine)
        {
            return (engine
                .FreePieces
                * _FREE_SCORE) +

            (engine
                .LegalMoves
                .Sum(m => m
                    .Value
                    .Count)
                * _LEGAL_SCORE) +

            (new[]
                {
                    PieceState.Air,
                    PieceState.Fire,
                    PieceState.Water,
                    PieceState.Earth
                }
                .Sum(s => (engine.Board.Count(t => t.item == s) % 2))
                * _ODD_ELEMENT_PENALTY) +

            (-engine
                .Board
                .Sum(t => _PIECE_SCORE[t.item]));
        }
    }
}
