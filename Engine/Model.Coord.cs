
using System;

namespace SigmarsGarden.Engine
{
    public struct Coord
    {
        public Int32 x;
        public Int32 y;

        public Coord(Int32 x, Int32 y)
        {
            this.x = x;
            this.y = y;
        }

        public override Boolean Equals(Object? obj) => obj != null && obj is Coord b && this.x == b.x && this.y == b.y;
        public override Int32 GetHashCode()
        {
            Int32 hash = 23;
            hash = hash * 31 + x.GetHashCode();
            hash = hash * 31 + y.GetHashCode();

            return hash;
        }

        public static Boolean operator== (Coord a, Coord b) => a.x == b.x && a.y == b.y;
        public static Boolean operator!= (Coord a, Coord b) => a.x != b.x || a.y != b.y;

        public override String ToString() => $"{{{x},{y}}}";
    }
}
