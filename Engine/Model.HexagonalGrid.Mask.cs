using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    //Mmmmmmmm https://www.redblobgames.com/grids/hexagons/
    public class HexagonalGridMask<T> : IHexagonalGrid<T>
    {
        private readonly IHexagonalGrid<T> _source;

        private Dictionary<Int32, T> _changes { get; } = new Dictionary<Int32, T>();

        public HexagonalGridMask(IHexagonalGrid<T> source)
        {
            _source = source;
        }

        public Int32 Width => _source.Width;
        public Int32 Height => _source.Height;

        public T this[Int32 position]
        {
            get => _changes.ContainsKey(position)
                ? _changes[position]
                : _source[position];
            set => _changes[position] = value;
        }
        public T this[Coord coord]
        {
            get
            {
                Int32 pos = _positionFromCoord(coord);
                return _changes.ContainsKey(pos)
                    ? _changes[pos]
                    : _source[coord];
            }
            set
            {
                if (coord.x < 0 || Width <= coord.x) throw new IndexOutOfRangeException($"X coordinate refers to a position outside of the grid. ({coord.x} < 0 || _width <= {coord.x}) ");
                if (coord.y < 0 || Height <= coord.y) throw new IndexOutOfRangeException($"Y coordinate refers to a position outside of the grid. ({coord.y} < 0 || _height <= {coord.y}) ");

                this[_positionFromCoord(coord)] = value;
            }
        }

        public void Clear() => throw new NotSupportedException($"Clear is not supported on a HexagonalGridMask.");

        private Int32 _positionFromCoord(Coord coord) => (coord.y * Width) + coord.x;

        public IEnumerator<(Coord coordinate, T item)> GetEnumerator() => _source
            .Select(t => _changes.ContainsKey(_positionFromCoord(t.coordinate))
                ? (t.coordinate, _changes[_positionFromCoord(t.coordinate)])
                : t)
            .GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
