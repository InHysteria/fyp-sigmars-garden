using System;
using System.IO;

namespace SigmarsGarden.Engine
{
    public class Snapshot
    {
        public Byte Move1 { get; }
        public Byte Move2 { get; }
        public Byte[] Board { get; }

        public Snapshot(Byte move1, Byte move2, Byte[] board) => (Move1, Move2, Board) = (move1, move2, board);

        public static Snapshot CreateFrom(IGameEngine engine, Move move)
        {
            Int32 move1 = (move.coord1.y * engine.Board.Width) + move.coord1.x;
            Int32 move2 = (move.coord2.y * engine.Board.Width) + move.coord2.x;
            Byte[] board = new Byte[91];

            Int32 i = 0;
            board[i++] = (Byte)engine.Board[new Coord(BoardGenerator.BOARD_SIZE - 1, BoardGenerator.BOARD_SIZE - 1)];
            foreach (Int32 pos in BoardGenerator.BoardPositions)
                board[i++] = (Byte)engine.Board[pos];

            return new Snapshot((Byte)move1, (Byte)move2, board);
        }

        public void Save(BinaryWriter writer)
        {
            writer.Write(Move1);
            writer.Write(Move2);
            writer.Write(Board, 0, 91);
        }
        public static Snapshot Load(BinaryReader reader) => new Snapshot(
            reader.ReadByte(),
            reader.ReadByte(),
            reader.ReadBytes(91)
        );
    }
}
