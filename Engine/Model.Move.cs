using System;

namespace SigmarsGarden.Engine
{
    public struct Move
    {
        public Coord coord1;
        public Coord coord2;

        public Move(Coord coord1, Coord coord2)
        {
            this.coord1 = coord1;
            this.coord2 = coord2;
        }

        public override Boolean Equals(Object? obj) => obj != null && obj is Move b && this.coord1 == b.coord1 && this.coord2 == b.coord2;
        public override Int32 GetHashCode()
        {
            Int32 hash = 23;
            hash = hash * 31 + coord1.GetHashCode();
            hash = hash * 31 + coord2.GetHashCode();

            return hash;
        }

        public static Boolean operator== (Move a, Move b) => a.coord1 == b.coord1 && a.coord2 == b.coord2;
        public static Boolean operator!= (Move a, Move b) => a.coord1 != b.coord1 || a.coord2 != b.coord2;

        public override String ToString() => $"{{{coord1},{coord2}}}";
    }
}
