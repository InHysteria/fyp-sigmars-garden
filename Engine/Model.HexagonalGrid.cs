using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public interface IHexagonalGrid<T> : IEnumerable<(Coord coordinate, T item)>
    {
        Int32 Width { get; }
        Int32 Height { get; }

        T this[Int32 position] { get; set; }
        T this[Coord coordinate] { get; set; }

        void Clear();
    }

    //Mmmmmmmm https://www.redblobgames.com/grids/hexagons/
    public class HexagonalGrid<T> : IHexagonalGrid<T>
    {
        private Int32 _width;
        private Int32 _height;
        private T[] _grid;
        private T _default;

        public HexagonalGrid(Int32 width, Int32 height, T @default)
        {
            _width = width;
            _height = height;
            _grid = new T[width * height];
            _default = @default;
        }

        public Int32 Width => _width;
        public Int32 Height => _height;

        public T this[Int32 position]
        {
            get
            {
                if (position < 0 || _width*_height <= position) return _default;

                return _grid[position];
            }
            set
            {
                if (position < 0 || _width*_height <= position) throw new IndexOutOfRangeException($"Position refers to am index outside of the grid. ({position} < 0 || {_width*_height} <= {position}) ");

                _grid[position] = value;
            }
        }
        public T this[Coord coordinate]
        {
            get
            {
                if (coordinate.x < 0 || _width <= coordinate.x) return _default;
                if (coordinate.y < 0 || _height <= coordinate.y) return _default;

                return this[(coordinate.y * _width) + coordinate.x];
            }
            set
            {
                if (coordinate.x < 0 || _width <= coordinate.x) throw new IndexOutOfRangeException($"X coordinate refers to a position outside of the grid. ({coordinate.x} < 0 || _width <= {coordinate.x}) ");
                if (coordinate.y < 0 || _height <= coordinate.y) throw new IndexOutOfRangeException($"Y coordinate refers to a position outside of the grid. ({coordinate.y} < 0 || _height <= {coordinate.y}) ");

                this[(coordinate.y * _width) + coordinate.x] = value;
            }
        }

        public void Clear() => Array.Clear(_grid, 0, _grid.Length);
        public HexagonalGrid<T> Clone()
        {
            HexagonalGrid<T> result = new HexagonalGrid<T>(_width, _height, _default);
            foreach ((Coord coordinate, T item) in this)
                result[coordinate] = item;

            return result;
        }

        private IEnumerable<(Coord coordinate, T item)> _gridEnumerable => _grid.Select((t,i) => (new Coord(i % _width, i / _width), t));
        public IEnumerator<(Coord coordinate, T item)> GetEnumerator() => _gridEnumerable.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => (_gridEnumerable as IEnumerable).GetEnumerator();
    }
}
