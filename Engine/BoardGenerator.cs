using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SigmarsGarden.Engine
{
    public class BoardGenerator : IBoardGenerator
    {
        public const Int32 BOARD_SIZE = 6;
        public const Int32 BOARD_WIDTH = BOARD_SIZE * 2 - 1;

        public const Int32 PATTERN_SIZE = 36;
        public const String PATTERN_FILE = "patterns.dat";

        public const Int32 GENERATE_PATTERN_COUNT = 50000;

        private Random _random;
        private readonly HashSet<Int32>[] _patterns;

        public static List<PieceState> BoardContents { get; private set; } = new (PieceState state, Int32 count)[]
        {
            (PieceState.Salt, 4),
            (PieceState.Air, 8),
            (PieceState.Fire, 8),
            (PieceState.Water, 8),
            (PieceState.Earth, 8),

            (PieceState.Vitae, 4),
            (PieceState.Mors, 4),

            (PieceState.Quicksilver, 5),
            (PieceState.Lead, 1),
            (PieceState.Tin, 1),
            (PieceState.Iron, 1),
            (PieceState.Copper, 1),
            (PieceState.Silver, 1),

            //(State.Gold, 1) //Gold is never positioned randomly and always resides at 0,0,0
        }
             //Generate a list of size t.count, filled with t.state
            .SelectMany(t => Enumerable
                .Range(0, t.count)
                .Select(_ => t.state))
            .ToList();

        public static readonly List<Int32> BoardPositions = Enumerable
            // Create a grid of coordinates shaped in a spiral up to BOARD_SIZE
            .Range(1, BOARD_SIZE - 1)
            .SelectMany(d => new[] {
                Enumerable
                    .Range(1, d)
                    .Select(i => (i, -d, d - i)),
                Enumerable
                    .Range(1, d)
                    .Select(i => (d, i - d, -i)),
                Enumerable
                    .Range(1, d)
                    .Select(i => (d - i, i, -d)),
                Enumerable
                    .Range(1, d)
                    .Select(i => (-i, d, i - d)),
                Enumerable
                    .Range(1, d)
                    .Select(i => (-d, d - i, i)),
                Enumerable
                    .Range(1, d)
                    .Select(i => (i - d, -i, d))
            })
            .SelectMany(x => x)
            .Select(_convertFromCube)
            .Take(91) //This should be unnessecary. Test.
            .ToList();

        public BoardGenerator(Random random)
        {
            _random = random;

            if (!File.Exists(PATTERN_FILE))
            {
                Console.WriteLine($"No patterns exist, generating {GENERATE_PATTERN_COUNT} new patterns..");
                using (FileStream fswrite = File.OpenWrite(PATTERN_FILE))
                    _generatePatterns(fswrite, _random, GENERATE_PATTERN_COUNT);
            }

            using (FileStream fs = File.OpenRead(PATTERN_FILE))
                _patterns = _loadPatterns(fs);
        }

        private static void _generatePatterns(Stream output, Random random, Int32 count)
        {
            using BinaryWriter writer = new BinaryWriter(output);

            writer.Write(count);
            for (Int32 i = 0; i < count; i++)
            {
                HashSet<Int32> pattern = new HashSet<Int32>();
                Boolean sixwaySymmetry =  random.Next() % 2 == 1;
                while (pattern.Count < PATTERN_SIZE)
                {
                    Int32 a = random.Next(0, BOARD_SIZE);
                    Int32 b = -random.Next(0, BOARD_SIZE);
                    Int32 c = -a-b;

                    if (a == 0 && b == 0) continue; //Never exclude the center space.

                    if (sixwaySymmetry)
                    {
                        pattern.Add(_convertFromCube((-a, -b, -c)));
                        pattern.Add(_convertFromCube((-b, -c, -a)));
                        pattern.Add(_convertFromCube((-c, -a, -b)));
                    }
                    pattern.Add(_convertFromCube((a, b, c)));
                    pattern.Add(_convertFromCube((b, c, a)));
                    pattern.Add(_convertFromCube((c, a, b)));
                }

                foreach (Int32 coord in pattern)
                    writer.Write(coord);
            }
        }

        private static HashSet<Int32>[] _loadPatterns(Stream output)
        {
            using BinaryReader reader = new BinaryReader(output);

            Int32 count = reader.ReadInt32();
            HashSet<Int32>[] result = new HashSet<Int32>[count];
            for (Int32 i = 0; i < count; i++)
            {
                result[i] = new HashSet<Int32>();
                for (Int32 p = 0; p < PATTERN_SIZE; p++)
                    result[i].Add(reader.ReadInt32());
            }

            return result;
        }


        public HexagonalGrid<PieceState> Next()
        {
            HexagonalGrid<PieceState> board = new HexagonalGrid<PieceState>(BOARD_SIZE*2 - 1, BOARD_SIZE*2 - 1, PieceState.Empty);
            HashSet<Int32> pattern = _patterns[_random.Next(0, _patterns.Length)];
            Int32 pieceCount = BoardContents.Count;

            //Shuffle board contents
            //See: https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
            PieceState tmp;
            for (Int32 i = 0; i < pieceCount; i++)
            {
                Int32 j = _random.Next(i, pieceCount);
                tmp = BoardContents[i];
                BoardContents[i] = BoardContents[j];
                BoardContents[j] = tmp;
            }

            board[new Coord(BOARD_SIZE - 1, BOARD_SIZE - 1)] = PieceState.Gold;

            foreach (Int32 c in BoardPositions)
            {
                if (pieceCount == 0) break;
                if (pattern.Contains(c)) continue;

                board[c] = BoardContents[--pieceCount];
            }

            if (pieceCount != 0)
                throw new Exception($"There were still pieces left to place after generation.");

            return board;
        }


        private static Int32 _convertFromCube((Int32 x, Int32 y, Int32 z) cube) =>
            ((BOARD_SIZE - 1) + cube.x + (cube.z - (cube.z & 1)) / 2) +  //X
            ((BOARD_SIZE - 1) + cube.z) * BOARD_WIDTH;                   //Y
    }
}
